//  Well done here, team!  You're calulator works nicely, has a simple, yet elegant GUI, and for the most part
// and has some good validations - super concise code!  I did notice a couple of issues:
// - I could enter multiple decimals in an operation which isn't really valid
// - I could enter text in the text box (that should have been disabled as text isn't valid in a calc)
// - I could enter multiple operations signs in a row, which then makes equals entirely ignore the operation
//  Otherwise things looks and worked great, and you have the comments I needed to see.
// 8/10

  var display = "";

  function addNumber(x) {

    display+=x.value;
    // + means adds to x value... example if i press 1 then 2, it will show 12

    document.getElementById('txtNumber').value = display;
  }

  function clearText() {
    document.getElementById("txtNumber").value = "";
    display = "";
  }

  function calculate () {
    var result = eval(display);
    if (result === Infinity) {
      alert("You can't devide by 0!");
      clearText();
    } else {
      document.getElementById('txtNumber').value = result;
    }
  }
//This is the script for our calculator
