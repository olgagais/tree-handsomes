

  var dispArray = [];
  var display;
  var sCounter = 0;
  var mCounter = 0;
  var prevX; //temporary holder for x.value in addNumber(x) function
  var timer;

  function calculateDisplay() {
    if(display.length < 4) {
      var rawClone = display.toString();
      for(i = 0; i < (4 - display.length);  i++) {
        rawClone = "0" + rawClone;
      }
      rawClone = rawClone.slice(0,2) + ":" + rawClone.slice(2,4);
      return rawClone;
    } else {
      display = display.slice(0,2) + ":" + display.slice(2,4);
      return display;
    }
  }

  function addNumber(val) {
    if(dispArray.length < 4) {
      dispArray[dispArray.length] = val.value;
      display = dispArray.join('');
      console.log(calculateDisplay());
    }
  }

  function clearText() {
    document.getElementById("txtNumber").value = "";
    display = "";
    //reset mCounter and sCounter
    mCounter = 0;
    sCounter = 0;
  }


  function startTimer(){
      //document.getElementById('txtNumber').innerHTML = (mCounter)+ ":" + (sCounter--);
      //document.getElementById('txtNumber').value = (mCounter)+ ":" + (sCounter--);
      //account for situations where user enters seconds>59
      if(sCounter >59){
        mCounter = mCounter + 1;
        sCounter = sCounter - 60;
      }
      //Display according to number of digits
      if(mCounter > 9 && sCounter > 9){
        document.getElementById("txtNumber").value = mCounter + ":" + sCounter--;
      }
      else if(mCounter > 9 && sCounter <= 9){
        document.getElementById("txtNumber").value = mCounter + ":0" + sCounter--;
      }
      else if(mCounter <= 9 && mCounter != 0 && sCounter > 9){
        document.getElementById("txtNumber").value = "0" + mCounter + ":" + sCounter--;
      }
      else if (mCounter <= 9 && mCounter != 0 && sCounter <= 9){
        document.getElementById("txtNumber").value = "0" + mCounter + ":0" + sCounter--;
      }
      else if(mCounter == 0 & sCounter > 9){
        document.getElementById("txtNumber").value = "00:" + sCounter--;
      }
      else {
        document.getElementById("txtNumber").value = "00:0" + sCounter--;
      }

      document.getElementById('startBtn').disabled=true;

      if(mCounter<0){
          stopTimer();
      }
      else if(sCounter<0){
         mCounter--;
         sCounter=59;
        timer = setTimeout("startTimer()",1000);
      }
      else{
          timer = setTimeout(startTimer,1000);
      }
  }

  function stopTimer(){
    clearTimeout(timer);
      // only show timer stopped if the user presses the "stop" button. If the user
      // did not press stop, then it is assumed the timer ran out.
      if (mCounter >= 0 && sCounter >= 0){
        document.getElementById('txtNumber').value = "Timer stopped";
      }
      else {
        //flash "Done" 3 times on display (setTimeout() functions are called
        // at the same time so need to delay the ms value by 1000ms for each sequential call).

        //Clear display first
        document.getElementById('txtNumber').value = "";

        for(i = 1; i < 7; i++) {
          if(i % 2 === 0) { //i is even
            setTimeout(displayEmpty,(i*1000));
          } else { //i is odd
            setTimeout(displayDone,(i*1000));
          }
        }

      }
      document.getElementById('startBtn').disabled=false;
  }
  function displayDone(){
    document.getElementById('txtNumber').value = "Done";
  }
  function displayEmpty(){
    document.getElementById('txtNumber').value = "";
  }
